os: linux
tag: user.tmux
-
mugs: "tmux "

#session management
mugs new session:
    insert('tmux new ')
mugs sessions:
    key(ctrl-b)
    key(s)
mugs name session:
    key(ctrl-b)
    key($)
mugs kill session:
    insert('tmux kill-session -t ')
#window management
mugs new window:
    key(ctrl-b)
    key(c)
mugs window <number>:
    key(ctrl-b )
    key('{number}')
mugs previous window:
    key(ctrl-b)
    key(p)
mugs next window:
    key(ctrl-b)
    key(n)
mugs rename window:
    key(ctrl-b)
    key(,)
mugs close window:
    key(ctrl-b)
    key(&)
#pane management
mugs split horizontal:
    key(ctrl-b)
    key(%)
mugs split vertical:
    key(ctrl-b)
    key(")
mugs next pane:
    key(ctrl-b)
    key(o)
mugs move <user.arrow_key>:
    key(ctrl-b)
    key(arrow_key)
mugs close pane:
    key(ctrl-b)
    key(x)
#Say a number right after this command, to switch to pane
mugs pane numbers:
    key(ctrl-b)
    key(q)
